function [Controller,cost_per_iteration] = Main_ILQG (Model_Param, Task, Controller)

cost_per_iteration = [];
helper_functions;

%% Design LQR controller to start with
A = A_ballbot(zeros(10,1),zeros(3,1));
B = B_ballbot(zeros(10,1),zeros(3,1));
R = diag([0.1 0.1 0.1]);
Q = diag([1 0.5 1 0.5 0.4 0.2 0.2 0.1 0.2 0.1]);

P = care(A,B,Q);
K_opt = real(R\B')*P;

N = ceil(Task.goal_time/Task.dt)+1;
for i = 1:N
    Controller.theta(:,:,i) = [zeros(1,3); -K_opt'];
end
state = zeros(10,1);
Controller.BaseFnc = @(state) [ones(1,size(state,2)); state];
Controller.time = (0:Task.dt:Task.goal_time);

%% simulate system with LQR controller
sim_out = Ballbot_Simulator_protected(Model_Param,Task,Controller);

%% Main loop for ILQG design iterations

for i = 1:Task.max_iteration
    % Initialize current iteration step
    S_k = Q_fun(sim_out.x(:,end),sim_out.u(:,end));
    s_k = q_fun(sim_out.x(:,end),sim_out.u(:,end));
    s_vec_k = q_vec_fun(sim_out.x(:,end),sim_out.u(:,end));
    
    u_old = sim_out.u;
    x_old = sim_out.x;
    
    % backwards calculating cost to go for current controller
    for k = length(sim_out.t)-1:-1:1

        r_k = r_fun(sim_out.x(:,k),sim_out.u(:,k));
        B_k = Task.dt*B_ballbot(sim_out.x(:,k),sim_out.u(:,k));
        P_k = P_fun(sim_out.x(:,k),sim_out.u(:,k));
        A_k = eye(10) + Task.dt*A_ballbot(sim_out.x(:,k),sim_out.u(:,k));
        R_k = R_fun(sim_out.x(:,k),sim_out.u(:,k));
        q_k = q_fun(sim_out.x(:,k),sim_out.u(:,k));
        q_vec_k = q_vec_fun(sim_out.x(:,k),sim_out.u(:,k));
        Q_k = Q_fun(sim_out.x(:,k),sim_out.u(:,k));
        c_k = c_fun(sim_out.x(:,k),sim_out.u(:,k));
        
        g = r_k + B_k'*s_vec_k; % C_i_k are zero
        G = P_k + B_k'*S_k*A_k;
        H = R_k + B_k'*S_k*B_k; % C_i_k are zero
        [V,D] = eig(H);
        for m = 1:size(D,1)
            if (D(m,m) < 1e-5)
                D(m,m) = 1e-5;
            end
        end
        H = real(V*D/V);
        H = 0.5*(H + H'); % make H symmetric

        % Update control law for next iteration i
        l_k = real(-H\g);
        L_k = real(-H\G);
        Controller.theta(1,:,k) = (u_old(:,k) + l_k - L_k*x_old(:,k))';
        Controller.theta(2:11,:,k) = L_k';
        
        % S, s and s_vec for next k
        s_k = q_k + s_k -0.5*g'*real(H\g);
        for c_iter = 1:size(c_k,2)
            s_k = s_k + 0.5*c_k(:,c_iter)'*S_k*c_k(:,c_iter);
        end
        
        S_k = Q_k + A_k'*S_k*A_k - G'*real(H\G);
        [V,D] = eig(S_k);
        for m = 1:size(D,1)
            if (D(m,m) < 1e-5)
                D(m,m) = 1e-5;
            end
        end
        S_k = real(V*D/V);
        S_k = 0.5*(S_k + S_k'); % make S_k symmetric

        s_vec_k = q_vec_k + A_k'*s_vec_k - G'*real(H\g);
        
    end
    
    cost_per_iteration(i) = s_k;
    
    % new simulation with the updated control law
    sim_out = Ballbot_Simulator_protected(Model_Param,Task,Controller);

    if (size(sim_out.x,2) ~= N)
        display('Robot fell!');
        break;
    elseif (sum(sum((sim_out.u - u_old).^2)) < 1e-1) % stop if controller does not change much anymore
        break;
    end

end


end

