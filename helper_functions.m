x_sym = sym(zeros(10,1));
u_sym = sym(zeros(3,1));
for cnt = 1:10
    x_sym(cnt)  = sym(sprintf('x%d',cnt),'real');
end
for cnt = 1:3
    u_sym(cnt)  = sym(sprintf('u%d',cnt),'real');
end

%% Create some helper functions
F = C_ballbot(x_sym,u_sym);
c_sym = sqrt(Task.dt).*F;
c_fun = matlabFunction(c_sym,'vars',{x_sym,u_sym});

r_sym = jacobian(Task.cost.l,u_sym)'*Task.dt;
r_fun = matlabFunction(r_sym,'vars',{x_sym,u_sym});

R_sym = jacobian(r_sym,u_sym);
R_fun = matlabFunction(R_sym,'vars',{x_sym,u_sym});

P_sym = jacobian(jacobian(Task.cost.l,u_sym)'*Task.dt,x_sym);
P_fun = matlabFunction(P_sym,'vars',{x_sym,u_sym});

q_sym = Task.dt*Task.cost.l;
q_fun = matlabFunction(q_sym,'vars',{x_sym,u_sym});

q_vec_sym = jacobian(Task.cost.l,x_sym)'*Task.dt;
q_vec_fun = matlabFunction(q_vec_sym,'vars',{x_sym,u_sym});

Q_sym = jacobian(q_vec_sym,x_sym);
Q_fun = matlabFunction(Q_sym,'vars',{x_sym,u_sym});