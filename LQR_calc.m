
%%

clear all;
close all;
clc;

%%


load('Task.mat');           %loading of the task structure
load('Model_Param.mat')     %loading of the model parameters structure

timedimension=1;

x_ini=Task.start_x;
u_ini=[0 0 0]';

simout.x=x_ini;
Controller.theta=zeros(10,3,timedimension);
Controller.BaseFnc=0;
Controller.time=[];





basefnc=@BaseFunction;

A=A_ballbot(x_ini,u_ini);
B=B_ballbot(x_ini,u_ini);
R=diag([0.1,0.1,0.1]);
Q= diag([1,0.5,1,0.5,0.4,0.2,0.2,0.1,0.2,0.1]);

P=care(A,B,Q);
Kopt=R^(-1)*B'*P;
    
for j=1:timedimension
   Controller.theta(:,:,j)=-Kopt';
end
    
Controller.BaseFnc=basefnc;
    
sim_out = Ballbot_Simulator_protected(Model_Param,Task,Controller);


showResults(Model_Param, sim_out);





