clc
clear all

%% Model_Param structure
% Mechanical parameters of Rezero
%
load Model_Param


%% Task structure
%
% .dt        : sampling time period.
% .start_x   : It is the initial state vector in time = start_time
% .start_time: It is the starting time for simulation.
% .goal_pos  : It is the 2D position of the ball center with
%              respect to the inertial frame at time = goal_time
% .goal_vel  : It is the velocity of the ball center with
%              respect to the inertial frame at time = goa_time
% .goal_time : It is the time at which the robot should have
%              reached to the goal.
% .max_iteration: This defines the maximum number of iterations
%                 for ILQG algorithm
% .random     : This is a batch of random sequences which will be
%               used in Rezero simulator.
%.cost        : It is a structure which contains the cost function
%                elements like h(x(T_f)) and l(x(t),u(t)).

Task = struct;
Task.dt         = 0.02;
Task.start_x    = rand(10,1)*0.1-0.05;
Task.start_time = 0;
Task.goal_pos   = [2 -0.8]';
Task.goal_vel   = [0 0]';
Task.goal_time  = 10;
Task.max_iteration = 20;
Task.random     = randn(3,Task.goal_time/Task.dt + 1,Task.max_iteration);

% Cost structure 
%
% .x: A real symbolic vector of system states. 
% .u: A real symbolic vector of system control inputs. 
% .h: Continuous-time terminal cost. It is a function of the state vector
% .l: Continuous-time immediate cost. It is a function of the state vector, 
%     and control input vector.

x_sym = sym(zeros(10,1));
u_sym = sym(zeros(3,1));
for cnt = 1:10
    x_sym(cnt)  = sym(sprintf('x%d',cnt),'real');
end
for cnt = 1:3
    u_sym(cnt)  = sym(sprintf('u%d',cnt),'real');
end
h = 100 *( (-0.125*x_sym(7)-Task.goal_pos(2))^2 + (0.125*x_sym(9)-Task.goal_pos(1))^2 ) + ...
    40*( (-0.125*x_sym(8)-Task.goal_vel(2))^2 + (0.125*x_sym(10)-Task.goal_vel(1))^2 ) + ...
    x_sym'*diag([40 4 40 4 40 4 0 0 0 0])*x_sym;
l = 10*x_sym(5)^2 + 1*x_sym(6)^2 + 0.1*(u_sym'*u_sym);

Cost   = struct;
Cost.x = x_sym;
Cost.u = u_sym;
Cost.h = h;
Cost.l = l;



Task.cost = Cost;


%% Controller structure
%
% u(x) = theta(t) ' * BaseFnc(x)
% .theta  : It is the controller parameters 3D-matrix which has
%           3 columns associated to each control input. The third
%           dimension is associated with the time. Each time step
%           has its specific level in this 3D matrix.
% .BaseFnc: Handle to base-function function
% .time   : This vector shows the relation between the 2D-matrix in
%           the third dimension of the *.theta matrix to the time step

Controller = struct;
Controller.theta   = zeros(11,3,ceil(Task.goal_time/Task.dt)+1);
x_feedback         = zeros(10,1);
Controller.BaseFnc = @(x_feedback) [ones(1,size(x_feedback,2)); x_feedback];
Controller.time    = [];

